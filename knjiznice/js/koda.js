
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var ehrTable = [];
var loggedIn = "";

/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  
  var ehrId = "";
  
  generateNewAccount();
  
  setTimeout(function() {
      generateNewData();
  }, 1000);
  
  
  setTimeout(function() {
      generateNewData();
  }, 2000);
  
  
  setTimeout(function() {
      generateNewData();
  }, 3000);
  
  
  
  return ehrId;
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

function generateNewAccount(){
    var month = randomIntFromInterval(1,12);
    var day = randomIntFromInterval(1,28);
    var hour = randomIntFromInterval(0,23);
    var minute = randomIntFromInterval(0,59);
    var time = (1940 + randomIntFromInterval(0,70)) + "-" + 
          (month  < 10 ? "0" + month : month ) + "-" +
          (day    < 10 ? "0" + day   : day   )  + "T" +
    	  (hour   < 10 ? "0" + hour  : hour  )  + ":" +
    	  (minute < 10 ? "0" + minute: minute)  + "Z";
    
    var name = ["Jessie", "Mark", "David", "John", "Steve", "Bobby", "Rick", "Shaq", "Anthony", "Morgan"];
    var surname = ["Black", "White", "Snow", "Miller", "Doe", "Washington", "Goodman", "Truman", "Grimes", "Kimmel"];
    document.getElementById("name").value = name[randomIntFromInterval(0,9)];
    document.getElementById("surname").value = surname[randomIntFromInterval(0,9)];
    document.getElementById("dateOfBirth").value = time;
    
    createAccount();
}

function generateNewData(){
      var month = randomIntFromInterval(1,12);
      var day = randomIntFromInterval(1,28);
      var hour = randomIntFromInterval(0,23);
      var minute = randomIntFromInterval(0,59);
      var time = (1940 + randomIntFromInterval(0,70)) + "-" + 
              (month  < 10 ? "0" + month : month ) + "-" +
              (day    < 10 ? "0" + day   : day   )  + "T" +
    		  (hour   < 10 ? "0" + hour  : hour  )  + ":" +
    		  (minute < 10 ? "0" + minute: minute)  + "Z";
      
      document.getElementById("weight").value = randomIntFromInterval(0,499);
      document.getElementById("height").value = randomIntFromInterval(0,76) + 137;
      document.getElementById("dateOfEntry").value = time;
      
      weightAndHeight();
}

function randomIntFromInterval(min,max){
    return Math.floor(Math.random()*(max-min+1)+min);
}

function createAccount(){
    sessionId = getSessionId();
    
    var name = document.getElementById("name").value;
    var surname = document.getElementById("surname").value;
    var dateOfBirth = document.getElementById("dateOfBirth").value;
    
    if( name != "" && surname != "" && dateOfBirth != "" ){
        
        $.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: name,
		            lastNames: surname,
		            dateOfBirth: dateOfBirth,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
		        };
				
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
    						ehrTable.push({ehr: ehrId, fn: name, ln: surname, bd: dateOfBirth});
							$("#registerMessage").html('										\
								<div style="text-align: center;" class="w3-text-black">			\
									<h4>Registration was successful!</h4>				    	\
									<h4>Your EHR ID is: </h4>' + ehrId + ' 						\
								</div>															\
							');
							$("#registeredUsers").append('<li><div onclick="showDetails(\'' + (document.getElementById("ehrId").value = ehrId) + '\')">' + name + ' ' + surname + ', <b>' + ehrId + '</b></div></li>');
		                }
		            },
		            error: function(err) {
						$("#registerMessage").html('									    	\
							<div style="text-align: center;" class="w3-text-black">		    	\
								<h4>Error!</h4>										        	\
								<h4>' + JSON.parse(err.responseText).userMessage + '</h4>   	\
							</div>														    	\
						');
		            }
		        });
		    }
		});
    }
    else{
        $("#registerMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You must fill out all of the boxes!</h4></div>');
    }
}

function login(){
    var ehr = document.getElementById("ehrId").value;
    var uslov = false;
    
    if( ehr != "" ){
        for(var i = 0; i < ehrTable.length; i++){
            if(ehr == ehrTable[i].ehr){
                loggedIn = ehr;
                uslov = true;
            }
        }
        if(uslov){
            displayData(false);
            $("#signInMessage").html('<div style="text-align:center" class="w3-text-black"><h4>Sign in successful!</h4></div>');
        }
        else{
            loggedIn = "";
            $("#signInMessage").html('<div style="text-align:center" class="w3-text-black"><h4>The user does not exist! Please check the EHR number and try again!</h4></div>');
        }
    }
    else{
        $("#signInMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You must fill out the box!</h4></div>');
    }
}

function addInformation(weight, height, time){
    loggedIn = document.getElementById("ehrId").value;
    if( height >= 136 && height <= 213 && weight > 0 && weight < 500 && loggedIn != ""){
        sessionId = getSessionId();
        
        $.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		
		var data = {
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": time,
		    "vital_signs/height_length/any_event/body_height_length": height,
		    "vital_signs/body_weight/any_event/body_weight": weight
		};
		
		var parameters = {
		    ehrId: loggedIn,
		    templateId: 'Vital Signs',
		    format: 'FLAT'
		};
		
		$.ajax({
		    url: baseUrl + "/composition?" + $.param(parameters),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(data),
		    success: function (res) {
				$("#fatOrFitStored").html('										\
					<div style="text-align: center;" class="w3-text-black">			\
						<h4>Data has been successfully stored!</h4>		           	\
					</div>															\
				');
		    }, 
		    error: function(err) {
				$("#fatOrFitStored").html('										\
					<div style="text-align: center;" class="w3-text-black">			\
						<h4>Error while storing data!</h4>							\
						<h4>' + JSON.parse(err.responseText).userMessage + '</h4>	\
					</div>															\
				');
		    }
		});
    }
}

function showDetails(ehrId) {
	document.getElementById("ehrId").value = ehrId;
	displayData(true);
	showDetailedData(ehrId);
	
	$("#pickedMessage").html('										    	\
		<div style="text-align: center;" class="w3-text-black">				\
			<h4>You have chosen the EHRID: ' + ehrId + '</h4>				\
		</div>																\
	');
}

function displayData(tf){
    
    sessionId = getSessionId();
	
	var ehrId = document.getElementById("ehrId").value;
	
	if (ehrId == "") {
		$("#signInMessage").html('											\
			<div style="text-align: center;" class="text-danger">			\
				<h4>Error!</h4>								      			\
				<h4>Fill out your EHR ID!</h4>				    			\
			</div>															\
		');
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: "GET",
			success: function(data) {
				if (!tf) {
					showDetails(ehrId);
				} else {
					var party = data.party;
					$("#signInMessage").html('																	        	\
						<div style="text-align: center;" class="w3-text-black">									        	\
							<h4>You have successfully signed in as: ' + party.firstNames + ' ' + party.lastNames + '!</h4>	\
						</div>																					        	\
					');

					$.ajaxSetup({
						headers: {"Ehr-Session": sessionId}
					});
					
					$.ajax({
						url: baseUrl + "/view/" + ehrId + "/height",
						type: "GET",
						success: function(heightData) {
							$("#signInDataMessage").html('														\
								<div style="text-align: center;" class="w3-text-black">							\
									<h4>You have ' + heightData.length + ' entries of data!</h4>	        	\
								</div>																			\
							');
						},
						error: function(err) {
							$("#signInMessage").html('											\
								<div style="text-align: center;" class="w3-text-black">			\
									<h4>Error!</h4>										    	\
									<h4>' + JSON.parse(err.responseText).userMessage + '</h4>	\
								</div>															\
							');
						}
					});
					
					$.ajax({
						url: baseUrl + "/view/" + ehrId + "/height",
						type: "GET",
						success: function(heightData) {
							var pts = [];
							for (var i = 0; i < heightData.length; i++) {
								pts.push({
									label: getDate(heightData[i].time),
									y: heightData[i].height
								});
							}
							for (var i = 0; i < pts.length / 2; i++) {
								var temp = pts[i];
								pts[i] = pts[pts.length-1-i];
								pts[pts.length-1-i] = temp;
							}
							var chart = new CanvasJS.Chart("heightGraph", {
								data: [              
									{
										type: "line",
										dataPoints: pts
									}
								]
							});
							chart.render();
						},
						error: function(err) {
							$("#heightGraph").html('											\
								<div style="text-align: center;" class="w3-text-black">			\
									<h4>Error!</h4>			    								\
									<h4>' + JSON.parse(err.responseText).userMessage + '</h4>	\
								</div>															\
							');
						}
					});
					
					$.ajax({
						url: baseUrl + "/view/" + ehrId + "/weight",
						type: "GET",
						success: function(weightData) {
							var pts = [];
							for (var i = 0; i < weightData.length; i++) {
								pts.push({
									label: getDate(weightData[i].time),
									y: weightData[i].weight
								});
							}
							for (var i = 0; i < pts.length / 2; i++) {
								var temp = pts[i];
								pts[i] = pts[pts.length-1-i];
								pts[pts.length-1-i] = temp;
							}
							var chart = new CanvasJS.Chart("weightGraph", {
								data: [              
									{
										type: "line",
										dataPoints: pts
									}
								]
							});
							chart.render();
						},
						error: function(err) {
							$("#weightGraph").html('											\
								<div style="text-align: center;" class="w3-text-black">			\
									<h4>Error!</h4>											\
									<h4>' + JSON.parse(err.responseText).userMessage + '</h4>	\
								</div>															\
							');
						}
					});
				}
			}, error: function(err) {
				$("#signInMessage").html('											\
					<div style="text-align: center;" class="text-danger">			\
						<h4>Error!</h4>											    \
						<h4>' + JSON.parse(err.responseText).userMessage + '</h4>	\
					</div>															\
				');
			}
		});
	}
}

function showDetailedData(ehrId){
    sessionId = getSessionId();
	
	var data = [];	
	
	$("#allInformationMessage").html('																	\
		<br>																							\
		<ol id="seznamVsehMeritev" style="text-align: center;" class="w3-text-teal">					\
		</ol>																							\
	');
	
	$.ajaxSetup({
		headers: {"Ehr-Session": sessionId}
	});
		
	$.ajax({
		url: baseUrl + "/view/" + ehrId + "/height",
		type: "GET",
		success: function(heightData) {
			for (var i = 0; i < heightData.length; i++) {
				data.push({
					date: getDate(heightData[i].time),
					height: heightData[i].height + ' ' + heightData[i].unit
				});
			}
			$.ajax({
				url: baseUrl + "/view/" + ehrId + "/weight",
				type: "GET",
				success: function(weightData) {
					for (var i = 0; i < weightData.length; i++) {
						for (var j = 0; j < data.length; j++) {
							if (data[j].date == getDate(weightData[i].time)) {
								data[j].weight = weightData[i].weight + ' ' + weightData[i].unit;
							}
						}
					}
				}, error: function(err) {
					$("#allInformationMessage").html('									\
						<div style="text-align: center;" class="w3-text-black">		    \
							<h4>Error!</h4>										    	\
							<h4>' + JSON.parse(err.responseText).userMessage + '</h4>	\
						</div>															\
					');
				}
			});
		}, error: function(err) {
			$("#allInformationMessage").html('								    \
				<div style="text-align: center;" class="w3-text-black">			\
					<h4>Error!</h4>											    \
					<h4>' + JSON.parse(err.responseText).userMessage + '</h4>	\
				</div>															\
			');
		}
	});
}

function getDate(date){
    return date.substring(0, 9);
}

function weightAndHeight(){
    var weight = document.getElementById("weight").value;
    var height = document.getElementById("height").value;
    var time = document.getElementById("dateOfEntry").value;
    
    if( weight != "" && height != "" && time != ""){
        addInformation(weight, height, time);
        if(height < 137){
            $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>There is no information for your height!</h4></div>');
        }
        else if(height >= 137 && height < 139){
            if(weight < 30.8){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are under the weight limit! Gain weight!</h4></div>');
            }
            else if(weight >= 30.8 && weight <= 38.1){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are in good shape! Keep it up!</h4></div>');
            }
            else if(weight > 38.1){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are over the weight limit! Lose weight!</h4></div>');
            }
            else{
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>Error! Please check the information and try again!</h4></div>');
            }
        }
        else if(height >= 140 && height < 142){
            if(weight < 28.5){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are under the weight limit! Gain weight!</h4></div>');
            }
            else if(weight >= 28.5 && weight <= 34.9){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are in good shape! Keep it up!</h4></div>');
            }
            else if(weight > 34.9){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are over the weight limit! Lose weight!</h4></div>');
            }
            else{
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>Error! Please check the information and try again!</h4></div>');
            }
            
        }
        else if(height >= 142 && height < 145){
            if(weight < 33.5){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are under the weight limit! Gain weight!</h4></div>');
            }
            else if(weight >= 33.5 && weight <= 40.8){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are in good shape! Keep it up!</h4></div>');
            }
            else if(weight > 40.8){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are over the weight limit! Lose weight!</h4></div>');
            }
            else{
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>Error! Please check the information and try again!</h4></div>');
            }
            
        }
        else if(height >= 145 && height < 147){
            if(weight < 35.8){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are under the weight limit! Gain weight!</h4></div>');
            }
            else if(weight >= 35.8 && weight <= 43.9){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are in good shape! Keep it up!</h4></div>');
            }
            else if(weight > 43.9){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are over the weight limit! Lose weight!</h4></div>');
            }
            else{
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>Error! Please check the information and try again!</h4></div>');
            }
            
        }
        else if(height >= 147 && height < 150){
            if(weight < 38.5){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are under the weight limit! Gain weight!</h4></div>');
            }
            else if(weight >= 38.5 && weight <= 46.7){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are in good shape! Keep it up!</h4></div>');
            }
            else if(weight > 46.7){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are over the weight limit! Lose weight!</h4></div>');
            }
            else{
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>Error! Please check the information and try again!</h4></div>');
            }
            
        }
        else if(height >= 150 && height < 152){
            if(weight < 40.8){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are under the weight limit! Gain weight!</h4></div>');
            }
            else if(weight >= 40.8 && weight <= 49.9){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are in good shape! Keep it up!</h4></div>');
            }
            else if(weight > 49.9){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are over the weight limit! Lose weight!</h4></div>');
            }
            else{
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>Error! Please check the information and try again!</h4></div>');
            }
            
        }
        else if(height >= 152 && height < 155){
            if(weight < 43.1){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are under the weight limit! Gain weight!</h4></div>');
            }
            else if(weight >= 43.1 && weight <= 53){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are in good shape! Keep it up!</h4></div>');
            }
            else if(weight > 53){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are over the weight limit! Lose weight!</h4></div>');
            }
            else{
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>Error! Please check the information and try again!</h4></div>');
            }
            
        }
        else if(height >= 155 && height < 157){
            if(weight < 45.8){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are under the weight limit! Gain weight!</h4></div>');
            }
            else if(weight >= 45.8 && weight <= 55.8){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are in good shape! Keep it up!</h4></div>');
            }
            else if(weight > 55.8){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are over the weight limit! Lose weight!</h4></div>');
            }
            else{
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>Error! Please check the information and try again!</h4></div>');
            }
            
        }
        else if(height >= 157 && height < 160){
            if(weight < 48.1){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are under the weight limit! Gain weight!</h4></div>');
            }
            else if(weight >= 48.1 && weight <= 58.9){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are in good shape! Keep it up!</h4></div>');
            }
            else if(weight > 58.9){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are over the weight limit! Lose weight!</h4></div>');
            }
            else{
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>Error! Please check the information and try again!</h4></div>');
            }
            
        }
        else if(height >= 160 && height < 163){
            if(weight < 50.8){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are under the weight limit! Gain weight!</h4></div>');
            }
            else if(weight >= 50.8 && weight <= 61.6){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are in good shape! Keep it up!</h4></div>');
            }
            else if(weight > 61.6){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are over the weight limit! Lose weight!</h4></div>');
            }
            else{
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>Error! Please check the information and try again!</h4></div>');
            }
            
        }
        else if(height >= 163 && height < 165){
            if(weight < 53){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are under the weight limit! Gain weight!</h4></div>');
            }
            else if(weight >= 53 && weight <= 64.8){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are in good shape! Keep it up!</h4></div>');
            }
            else if(weight > 64.8){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are over the weight limit! Lose weight!</h4></div>');
            }
            else{
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>Error! Please check the information and try again!</h4></div>');
            }
            
        }
        else if(height >= 165 && height < 168){
            if(weight < 55.3){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are under the weight limit! Gain weight!</h4></div>');
            }
            else if(weight >= 55.3 && weight <= 68){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are in good shape! Keep it up!</h4></div>');
            }
            else if(weight > 68){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are over the weight limit! Lose weight!</h4></div>');
            }
            else{
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>Error! Please check the information and try again!</h4></div>');
            }
            
        }
        else if(height >= 168 && height < 170){
            if(weight < 58){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are under the weight limit! Gain weight!</h4></div>');
            }
            else if(weight >= 58 && weight <= 70.7){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are in good shape! Keep it up!</h4></div>');
            }
            else if(weight > 70.7){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are over the weight limit! Lose weight!</h4></div>');
            }
            else{
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>Error! Please check the information and try again!</h4></div>');
            }
        }
        else if(height >= 170 && height < 173){
            if(weight < 60.3){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are under the weight limit! Gain weight!</h4></div>');
            }
            else if(weight >= 60.3 && weight <= 73.9){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are in good shape! Keep it up!</h4></div>');
            }
            else if(weight > 73.9){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are over the weight limit! Lose weight!</h4></div>');
            }
            else{
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>Error! Please check the information and try again!</h4></div>');
            }
        }
        else if(height >= 173 && height < 175){
            if(weight < 63){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are under the weight limit! Gain weight!</h4></div>');
            }
            else if(weight >= 63 && weight <= 76.6){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are in good shape! Keep it up!</h4></div>');
            }
            else if(weight > 76.6){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are over the weight limit! Lose weight!</h4></div>');
            }
            else{
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>Error! Please check the information and try again!</h4></div>');
            }
        }
        else if(height >= 175 && height < 178){
            if(weight < 65.3){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are under the weight limit! Gain weight!</h4></div>');
            }
            else if(weight >= 65.3 && weight <= 79.8){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are in good shape! Keep it up!</h4></div>');
            }
            else if(weight > 79.8){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are over the weight limit! Lose weight!</h4></div>');
            }
            else{
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>Error! Please check the information and try again!</h4></div>');
            }
        }
        else if(height >= 178 && height < 180){
            if(weight < 67.65){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are under the weight limit! Gain weight!</h4></div>');
            }
            else if(weight >= 67.6 && weight <= 83){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are in good shape! Keep it up!</h4></div>');
            }
            else if(weight > 83){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are over the weight limit! Lose weight!</h4></div>');
            }
            else{
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>Error! Please check the information and try again!</h4></div>');
            }
        }
        else if(height >= 70.3 && height < 85.7){
            if(weight < 72.6){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are under the weight limit! Gain weight!</h4></div>');
            }
            else if(weight >= 72.6 && weight <= 88.9){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are in good shape! Keep it up!</h4></div>');
            }
            else if(weight > 88.9){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are over the weight limit! Lose weight!</h4></div>');
            }
            else{
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>Error! Please check the information and try again!</h4></div>');
            }
        }
        else if(height >= 183 && height < 185){
            if(weight < 75.3){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are under the weight limit! Gain weight!</h4></div>');
            }
            else if(weight >= 75.3 && weight <= 91.6){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are in good shape! Keep it up!</h4></div>');
            }
            else if(weight > 91.6){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are over the weight limit! Lose weight!</h4></div>');
            }
            else{
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>Error! Please check the information and try again!</h4></div>');
            }
        }
        else if(height >= 185 && height < 188){
            if(weight < 77.5){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are under the weight limit! Gain weight!</h4></div>');
            }
            else if(weight >= 77.5 && weight <= 94.8){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are in good shape! Keep it up!</h4></div>');
            }
            else if(weight > 94.8){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are over the weight limit! Lose weight!</h4></div>');
            }
            else{
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>Error! Please check the information and try again!</h4></div>');
            }
        }
        else if(height >= 188 && height < 191){
            if(weight < 79.8){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are under the weight limit! Gain weight!</h4></div>');
            }
            else if(weight >= 79.8 && weight <= 98){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are in good shape! Keep it up!</h4></div>');
            }
            else if(weight > 98){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are over the weight limit! Lose weight!</h4></div>');
            }
            else{
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>Error! Please check the information and try again!</h4></div>');
            }
        }
        else if(height >= 191 && height < 193){
            if(weight < 82.5){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are under the weight limit! Gain weight!</h4></div>');
            }
            else if(weight >= 82.5 && weight <= 100.6){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are in good shape! Keep it up!</h4></div>');
            }
            else if(weight > 100.6){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are over the weight limit! Lose weight!</h4></div>');
            }
            else{
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>Error! Please check the information and try again!</h4></div>');
            }
        }
        else if(height >= 193 && height < 195){
            if(weight < 84.8){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are under the weight limit! Gain weight!</h4></div>');
            }
            else if(weight >= 84.8 && weight <= 103.8){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are in good shape! Keep it up!</h4></div>');
            }
            else if(weight > 103.8){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are over the weight limit! Lose weight!</h4></div>');
            }
            else{
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>Error! Please check the information and try again!</h4></div>');
            }
        }
        else if(height >= 195 && height < 198){
            if(weight < 87.5){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are under the weight limit! Gain weight!</h4></div>');
            }
            else if(weight >= 87.5 && weight <= 106.5){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are in good shape! Keep it up!</h4></div>');
            }
            else if(weight > 106.5){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are over the weight limit! Lose weight!</h4></div>');
            }
            else{
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>Error! Please check the information and try again!</h4></div>');
            }
        }
        else if(height >= 198 && height < 201){
            if(weight < 89.8){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are under the weight limit! Gain weight!</h4></div>');
            }
            else if(weight >= 89.8 && weight <= 109.7){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are in good shape! Keep it up!</h4></div>');
            }
            else if(weight > 109.7){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are over the weight limit! Lose weight!</h4></div>');
            }
            else{
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>Error! Please check the information and try again!</h4></div>');
            }
        }
        else if(height >= 201 && height < 203){
            if(weight < 92){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are under the weight limit! Gain weight!</h4></div>');
            }
            else if(weight >= 92 && weight <= 112.9){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are in good shape! Keep it up!</h4></div>');
            }
            else if(weight > 112.9){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are over the weight limit! Lose weight!</h4></div>');
            }
            else{
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>Error! Please check the information and try again!</h4></div>');
            }
        }
        else if(height >= 203 && height < 205){
            if(weight < 94.8){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are under the weight limit! Gain weight!</h4></div>');
            }
            else if(weight >= 94.8 && weight <= 115.6){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are in good shape! Keep it up!</h4></div>');
            }
            else if(weight > 115.6){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are over the weight limit! Lose weight!</h4></div>');
            }
            else{
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>Error! Please check the information and try again!</h4></div>');
            }
        }
        else if(height >= 205 && height < 208){
            if(weight < 97){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are under the weight limit! Gain weight!</h4></div>');
            }
            else if(weight >= 97 && weight <= 118.9){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are in good shape! Keep it up!</h4></div>');
            }
            else if(weight > 118.9){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are over the weight limit! Lose weight!</h4></div>');
            }
            else{
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>Error! Please check the information and try again!</h4></div>');
            }
        }
        else if(height >= 208 && height < 210){
            if(weight < 99.8){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are under the weight limit! Gain weight!</h4></div>');
            }
            else if(weight >= 99.8 && weight <= 121.5){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are in good shape! Keep it up!</h4></div>');
            }
            else if(weight > 121.5){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are over the weight limit! Lose weight!</h4></div>');
            }
            else{
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>Error! Please check the information and try again!</h4></div>');
            }
        }
        else if(height >= 210 && height < 213){
            if(weight < 102){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are under the weight limit! Gain weight!</h4></div>');
            }
            else if(weight >= 102 && weight <= 124.7){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are in good shape! Keep it up!</h4></div>');
            }
            else if(weight > 124.7){
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You are over the weight limit! Lose weight!</h4></div>');
            }
            else{
                $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>Error! Please check the information and try again!</h4></div>');
            }
        }
        else if(height >= 213){
            $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>There is no information for your height!</h4></div>');
        }
        else{
            $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>Error! please check your input again!</h4></div>');
        }
    }
    else{
        $("#fatOrFitMessage").html('<div style="text-align:center" class="w3-text-black"><h4>You must fill out all of the boxes!</h4></div>');
    }
}

$(document).ready(function() {
	$("#register").click(function() {
		createAccount();
	});
	
	$("#signIn").click(function() {
		login();
	});
	
	$("#inputInformation").click(function() {
		weightAndHeight();
	});
	
	$("#generiraj").click(function() {
	    generirajPodatke(1);
	    setTimeout(function(){
            generirajPodatke(1);
            setTimeout(function(){
                generirajPodatke(1);
            }, 8000);
        }, 16000);
	});
});
